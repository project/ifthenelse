api = 2
core = 8.x

; Modules

projects[default_content][version] = 1.0-alpha8
projects[default_content][subdir] = contrib

projects[admin_toolbar][version] = 1.27
projects[admin_toolbar][subdir] = contrib

projects[if_then_else][version] = 1.x-dev
projects[if_then_else][subdir] = contrib
