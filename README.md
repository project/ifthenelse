Stratus Meridian Developer Portal

TABLE OF CONTENTS
-----------------

* Introduction
* Requirements
* Installation
* Configuration

INTRODUCTION
------------
 
 "If Then Else" module provides a graphical user interface to set different rules which can work as a replacement of programmatic hook based approach to add custom actions on different hooks and events. 

 * For a full description of the profile, visit the project page:
   https://www.drupal.org/project/ 
 
REQUIREMENTS
------------
 
 * Drupal 8
 
INSTALLATION
------------
 
 Install as you would normally install a contributed Profile. 

CONFIGURATION
-------------

 * Configuration from here /admin/config/system/ifthenelse-rules
